package utils;


public class Rounder {
    public static String roundValue(double result){
        String rounder = String.format("%.2f",result);
        return rounder;
    }
}
