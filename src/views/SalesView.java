package views;

import models.Product;
import utils.Validator;

import java.beans.PropertyDescriptor;
import java.util.Scanner;

// View
public class SalesView {


    String title;
    String name;
    int quantity;
    double price;
    Scanner scanner;

    // Здесь, создайте конструктор данного класса,
    // который в параметре содержит переменную типа модели.
    Product model;

    public SalesView(Product model) {
        this.model = model;

    }

    public void getInputs() {

        scanner = new Scanner(System.in);

        // Здесь, реализуйте вывод сообщения о необходимсоти
        // ввода соответствующего значения, валидацью значения
        // через валидатор, установку валидного значения через модель.

        title = "Enter product name: ";
        System.out.println(title);
        model.setName(Validator.validateName(scanner));

        // Здесь, реализуйте вывод сообщения о необходимсоти
        // ввода соответствующего значения, валидацью значения
        // через валидатор, установку валидного значения через модель.

        title = "Enter quantity: ";
        System.out.println(title);
        model.setQuantity(Validator.validateQuantityInput(scanner));

        // Здесь, реализуйте вывод сообщения о необходимсоти
        // ввода соответствующего значения, валидацью значения
        // через валидатор, установку валидного значения через модель.

        title = "Enter price: ";
        System.out.println(title);
        model.setPrice(Validator.validatePriceInput(scanner));


        scanner.close();
    }

    public void getOutput(String output) {
        System.out.println(output);
    }
}
