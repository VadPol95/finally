package base;

public interface Income {
    double incomeCalculation(); // доход
    double incomeCalculation(double incomeValue, double taxRate); // чистый доход

}
