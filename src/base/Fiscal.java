package base;

public interface Fiscal {
    double incomeTaxCalculation(double incomeValue); // Расчет налога от дохода
}
