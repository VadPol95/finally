package controllers;

import models.Product;
import utils.Rounder;
import views.SalesView;


// Controller
public class ProductController{

    Product model;
    SalesView view;

    // Конструктор
    public ProductController(Product model, SalesView view) {
        this.model = model;
        this.view = view;
    }


    public void runApp() {

        view.getInputs();

        // Здесь, реализуйте:
        // 1) получение имени товара через модель;

        String name = model.getName();

        // 2) вызов методов расчетов доходов и налога;

        double income = model.incomeCalculation();
        double tax = model.incomeTaxCalculation(income);
        double result = model.incomeCalculation(income, tax);

        // 3) округление расчетных значений;

        String rounderIncome = Rounder.roundValue(income);
        String rounderTax = Rounder.roundValue(tax);
        String rounderNetIncome = Rounder.roundValue(result);

        // 4) вывод расчетов по заданному формату.

        String output = "Name of product: " + name + "\nTotal income: " + rounderIncome + "\nTax amount: " +
                rounderTax + "\nNet income: " + rounderNetIncome;

        view.getOutput(output);
    }
}
